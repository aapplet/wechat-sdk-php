# wechat-sdk-php

## 概览

- [x] 微信支付API v3
- [x] 微信公众平台
- [x] 微信小程序
- [ ] 跨城容灾
- [x] 自动更新微信支付平台证书
- [ ] 自动更新公众平台AccessToken

## 环境要求

- php>=7.4

## 安装

### Composer

```shell
composer require mele/wechat-sdk-php
```

## 初始化

```php
public function client(): DefaultWeChatClient
{
    $weChatConfig                 = new WeChatConfig();
    $weChatConfig->appId          = "..........Appid..........";
    $weChatConfig->appSecret      = "..........AppSecret......";
    $weChatConfig->mchId          = "..........MchId..........";
    $weChatConfig->mchKey         = "..........MchKey.........";
    $weChatConfig->serviceId      = "..........ServiceId......";
    $weChatConfig->serialNo       = "..........serialNo.......";
    $weChatConfig->privateKey     = file_get_contents('certs/privateKey.pem');
    // 自定义平台证书获取方法
    $weChatConfig->getCertificate = function ($serialNumber) {
        // return @file_get_contents($serialNumber);
        return Cache::get($serialNumber);
    };
    // 自定义平台证书保存方法
    $weChatConfig->setCertificate = function ($serialNumber, $certificate, $expireTime) {
        //return @file_put_contents($serialNumber, $certificateText);
        return Cache::set($serialNumber, $certificate, 3600);
    };
    
    return new DefaultWeChatClient($weChatConfig);
}
```

## 示例

### 查询订单API

```php
$client  = $this->client();
$execute = $client->v3(new WeChatPaymentQueryRequest('xxx'));
dd($execute);
```

## 联系我们

如果你发现了**BUG**或者有任何疑问、建议，请通过issue进行反馈。