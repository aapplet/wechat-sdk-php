<?php

namespace WeChat;

use WeChat\Base\WeChatClient;
use WeChat\Base\WeChatRequest;
use WeChat\Config\WeChatConfig;
use WeChat\Exception\WeChatResponseException;
use WeChat\Exception\WeChatValidationException;
use WeChat\Http\WeChatPaymentHeaders;
use WeChat\Http\WeChatPaymentRequest;
use WeChat\Http\WeChatPlatformRequest;
use WeChat\Validator\WeChatValidator;

class DefaultWeChatClient implements WeChatClient
{

    /**
     * @var WeChatConfig 配置文件
     */
    public WeChatConfig $config;

    public function __construct(WeChatConfig $config)
    {
        $this->config = $config;
    }

    function mp(WeChatRequest $request)
    {
        $config       = $this->config;
        $httpRequest  = new WeChatPlatformRequest($config, $request);
        $httpResponse = $httpRequest->execute();

        return json_decode($httpResponse->getBody());
    }

    function v3(WeChatRequest $request)
    {
        $config = $this->config;

        $httpRequest  = new WeChatPaymentRequest($config, $request);
        $httpResponse = $httpRequest->execute();

        $body       = $httpResponse->getBody();
        $headers    = $httpResponse->getHeaders();
        $statusCode = $httpResponse->getStatusCode();

        $json = json_decode($body);

        $paymentHeaders = new WeChatPaymentHeaders($headers);
        if ($paymentHeaders->emptySerialNumber())
        {
            throw new WeChatValidationException("微信[HTTP]请求失败 => " . $json->message);
        }
        $weChatValidator = new WeChatValidator($config, $paymentHeaders, $body);
        if (!$weChatValidator->verifyResponse())
        {
            throw new WeChatValidationException("微信[签名验证]验签失败");
        }
        if ($statusCode === 200 || $statusCode === 204)
        {
            return $json;
        }
        throw new WeChatResponseException($json->message);
    }

}
