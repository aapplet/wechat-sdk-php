<?php

namespace WeChat\Attribute;

class WeChatPlatformAttribute extends WeChatAttribute
{

    /**
     * 公众平台
     */
    public string $host = 'https://api.weixin.qq.com';

}
