<?php

namespace WeChat\Attribute;

class WeChatPaymentAttribute extends WeChatAttribute
{

    /**
     * 商户平台
     */
    public string $host = 'https://api.mch.weixin.qq.com';

}
