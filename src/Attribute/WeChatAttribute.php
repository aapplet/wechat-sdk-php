<?php

namespace WeChat\Attribute;

/**
 * 请求属性
 */
abstract class WeChatAttribute
{
    /**
     * 请求域名
     */
    protected string $host;
    /**
     * 请求路径
     */
    protected string $path;
    /**
     * 请求方法
     */
    protected string $method;
    /**
     * 请求参数
     */
    protected string $parameters = '';
    /**
     * 请求体
     */
    protected string $requestBody = '';

    /**
     * @param   string  $path  请求路径
     */
    public function setPath(string $path): void
    {
        $this->path = $path;
    }

    /**
     * @param   string  $method  请求方法
     */
    public function setMethod(string $method): void
    {
        $this->method = $method;
    }

    /**
     * @param   string  $parameters  请求参数
     */
    public function setParameters(string $parameters): void
    {
        $this->parameters = $parameters;
    }

    /**
     * @param   string  $requestBody  请求体
     */
    public function setRequestBody(string $requestBody): void
    {
        $this->requestBody = $requestBody;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * 资源路径(路径+参数)
     */
    public function getSignatureURL(): string
    {
        return $this->parameters === '' ? $this->path : $this->path . "?" . $this->parameters;
    }

    /**
     * 请求URL(域名+路径+参数)
     */
    public function getRequestURL(): string
    {
        return $this->host . $this->getSignatureURL();
    }

    /**
     * 请求体
     */
    public function getRequestBody(): string
    {
        return $this->requestBody;
    }

}
