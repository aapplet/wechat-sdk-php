<?php

namespace WeChat\Base;

use WeChat\Attribute\WeChatAttribute;
use WeChat\Config\WeChatConfig;

interface WeChatRequest
{

	/**
	 * 获取请求属性
	 *
	 * @param   WeChatConfig  $config  配置文件
	 *
	 * @return WeChatAttribute 请求属性
	 */
	public function getAttribute(WeChatConfig $config): WeChatAttribute;

}
