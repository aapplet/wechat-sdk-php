<?php

namespace WeChat\Base;

interface WeChatClient
{

    /**
     * 公众平台请求
     *
     */
    function mp(WeChatRequest $request);

    /**
     * 商户平台请求
     *
     */
    function v3(WeChatRequest $request);

}
