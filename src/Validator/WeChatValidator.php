<?php

namespace WeChat\Validator;

use WeChat\Cert\WeChatCertificateService;
use WeChat\Config\WeChatConfig;
use WeChat\Http\WeChatPaymentHeaders;

class WeChatValidator
{
    /**
     * 配置信息
     */
    protected WeChatConfig $config;
    /**
     * 响应头
     */
    protected WeChatPaymentHeaders $headers;
    /**
     * 响应体
     */
    protected string $body;

    public function __construct(WeChatConfig $config, WeChatPaymentHeaders $headers, string $body)
    {
        $this->config  = $config;
        $this->headers = $headers;
        $this->body    = $body;
    }

    /**
     * 签名验证
     *
     * @param   string  $certificate  平台证书
     *
     * @return bool 验证结果
     */
    public function verify(string $certificate): bool
    {
        $signature = $this->headers->getSignature();
        $timestamp = $this->headers->getTimestamp();
        $nonce     = $this->headers->getNonce();
        $body      = $this->body;

        $message = "$timestamp\n$nonce\n$body\n";

        return @openssl_verify($message, base64_decode($signature), openssl_x509_read($certificate), OPENSSL_ALGO_SHA256);
    }

    /**
     * 签名验证
     * @return bool 验证结果
     */
    public function verifyResponse(): bool
    {
        $config       = $this->config;
        $serialNumber = $this->headers->getSerial();

        $certificateService = new WeChatCertificateService($config);
        $certificate        = $certificateService->getCertificate($serialNumber);

        return $this->verify($certificate);
    }

}
