<?php

namespace WeChat\Cert;

use WeChat\Config\WeChatConfig;
use WeChat\Exception\WeChatException;

/**
 * 获取平台证书
 * <p>
 * https://pay.weixin.qq.com/wiki/doc/apiv3/apis/wechatpay5_1.shtml
 */
class WeChatCertificateService
{
    /**
     * @var WeChatConfig 配置文件
     */
    private WeChatConfig $config;

    public function __construct(WeChatConfig $config)
    {
        $this->config = $config;
    }

    /**
     * 获取平台证书
     *
     * @param   string  $serialNumber  平台证书序列号
     *
     * @return string 平台证书
     */
    function getCertificate(string $serialNumber): string
    {
        $config         = $this->config;
        $getCertificate = $config->getCertificate;

        if ($getCertificate && $certificate = $getCertificate($serialNumber))
        {
            return $certificate;
        }

        $certificateLoader = new WeChatCertificateLoader($config);
        $certificates      = $certificateLoader->loadCertificate();

        $stdClass = $certificates[$serialNumber] ?? null;
        if ($stdClass == null)
        {
            throw new WeChatException('微信[平台证书]找不到匹配的证书 => ' . $serialNumber);
        }

        return $stdClass->certificate;
    }

}
