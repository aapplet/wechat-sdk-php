<?php

namespace WeChat\Cert;

use SodiumException;
use stdClass;
use WeChat\Config\WeChatConfig;
use WeChat\Exception\WeChatException;
use WeChat\Exception\WeChatHttpException;
use WeChat\Exception\WeChatValidationException;
use WeChat\Http\WeChatPaymentHeaders;
use WeChat\Http\WeChatPaymentRequest;
use WeChat\Validator\WeChatValidator;

/**
 * 平台证书加载器
 * <p>
 * https://pay.weixin.qq.com/wiki/doc/apiv3/apis/wechatpay5_1.shtml
 */
class WeChatCertificateLoader
{

    /**
     * @var WeChatConfig 配置文件
     */
    private WeChatConfig $config;

    public function __construct(WeChatConfig $config)
    {
        $this->config = $config;
    }

    /**
     * 加载平台证书
     */
    public function loadCertificate(): array
    {
        $config = $this->config;

        $httpRequest  = new WeChatPaymentRequest($config, new WeChatCertificateRequest());
        $httpResponse = $httpRequest->execute();

        $body       = $httpResponse->getBody();
        $headers    = $httpResponse->getHeaders();
        $statusCode = $httpResponse->getStatusCode();

        if ($statusCode !== 200)
        {
            throw new WeChatHttpException("微信[平台证书]请求失败 => " . $body);
        }

        $jsons = json_decode($body)->data;

        $array = [];
        foreach ($jsons as $json)
        {
            $serialNo           = $json->serial_no;
            $expireTime         = $json->expire_time;
            $encryptCertificate = $json->encrypt_certificate;

            $stdClass              = new stdClass();
            $stdClass->expireTime  = $expireTime;
            $stdClass->certificate = $this->decryptCertificate($encryptCertificate);

            $array[$serialNo] = $stdClass;
        }

        $paymentHeaders = new WeChatPaymentHeaders($headers);
        $validator      = new WeChatValidator($config, $paymentHeaders, $body);

        $certificate = $array[$paymentHeaders->getSerial()]->certificate;
        if (!$validator->verify($certificate))
        {
            throw new WeChatValidationException("微信[平台证书]验签失败");
        }

        $getCertificate = $config->getCertificate;
        $setCertificate = $config->setCertificate;
        if ($getCertificate && $setCertificate)
        {
            foreach ($array as $key => $value)
            {
                $setCertificate($key, $value->certificate, $value->expireTime);
            }
        }

        return $array;
    }

    /**
     * 解码平台证书
     *
     * @param   stdClass  $encryptCertificate  证书密文
     *
     * @return string 平台证书
     */
    private function decryptCertificate(stdClass $encryptCertificate): string
    {
        $mchKey = $this->config->mchKey;

        $nonce          = $encryptCertificate->nonce;
        $algorithm      = $encryptCertificate->algorithm;
        $ciphertext     = $encryptCertificate->ciphertext;
        $associatedData = $encryptCertificate->associated_data;

        if ($algorithm !== 'AEAD_AES_256_GCM')
        {
            throw new WeChatException("微信[平台证书]不支持的算法 => " . $algorithm);
        }

        try
        {
            $certificateText = sodium_crypto_aead_aes256gcm_decrypt(
                base64_decode($ciphertext),
                $associatedData,
                $nonce,
                $mchKey
            );

            if ($certificateText === false)
            {
                throw new WeChatException("微信[平台证书]解码失败，请检查解码数据是否正确");
            }

            return $certificateText;
        }
        catch (SodiumException $e)
        {
            throw new WeChatException("微信[平台证书]解码异常，请检查商户APIv3密钥 => " . $e->getMessage());
        }
    }

}
