<?php

namespace WeChat\Cert;

use WeChat\Attribute\WeChatAttribute;
use WeChat\Attribute\WeChatPaymentAttribute;
use WeChat\Base\WeChatRequest;
use WeChat\Config\WeChatConfig;

/**
 * 平台证书请求属性
 * <p>
 * https://pay.weixin.qq.com/wiki/doc/apiv3/apis/wechatpay5_1.shtml
 */
class WeChatCertificateRequest implements WeChatRequest
{

    public function getAttribute(WeChatConfig $config): WeChatAttribute
    {
        $attribute = new WeChatPaymentAttribute();
        $attribute->setMethod('GET');
        $attribute->setPath('/v3/certificates');

        return $attribute;
    }

}
