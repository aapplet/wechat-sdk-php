<?php

namespace WeChat\Http;

use WeChat\Attribute\WeChatAttribute;
use WeChat\Base\WeChatRequest;
use WeChat\Config\WeChatConfig;
use WeChat\Exception\WeChatParamsException;

class WeChatPlatformRequest extends WeChatHttpRequest
{
    private WeChatConfig $config;
    private WeChatAttribute $attribute;

    /**
     * @param   WeChatConfig   $config   配置信息
     * @param   WeChatRequest  $request  请求信息
     */
    public function __construct(WeChatConfig $config, WeChatRequest $request)
    {
        if (empty($config->appId))
        {
            throw new WeChatParamsException('appId不能为空');
        }
        if (empty($config->appSecret))
        {
            throw new WeChatParamsException('appSecret不能为空');
        }
        $this->config    = $config;
        $this->attribute = $request->getAttribute($config);
    }

    public function execute(): WeChatHttpResponse
    {
        $this->method = $this->attribute->getMethod();
        $this->url    = $this->attribute->getRequestURL();
        $this->body   = $this->attribute->getRequestBody();

        return $this->send();
    }

}
