<?php

namespace WeChat\Http;

class WeChatHttpResponse
{

    /**
     * @var string 响应体
     */
    protected string $body;

    /**
     * @var array 响应头
     */
    protected array $headers;

    /**
     * @var int http状态码
     */
    protected int $statusCode;

    /**
     * @param   string  $body        响应体
     * @param   array   $headers     响应头
     * @param   int     $statusCode  http状态码
     */
    public function __construct(string $body, array $headers, int $statusCode)
    {
        $this->body       = $body;
        $this->headers    = $headers;
        $this->statusCode = $statusCode;
    }

    /**
     * @return string 响应体
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @return array 响应头
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @return int http状态码
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

}
