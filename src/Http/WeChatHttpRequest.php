<?php

namespace WeChat\Http;

use WeChat\Exception\WeChatHttpException;

class WeChatHttpRequest
{

    /**
     * @var string 请求url
     */
    protected string $url;
    /**
     * @var string 请求方式
     */
    protected string $method;
    /**
     * @var array 请求头
     */
    protected array $headers = [];
    /**
     * @var string 请求体
     */
    protected string $body = '';
    /**
     * @var int 允许cURL函数执行的最大秒数。
     */
    protected int $requestTimeout = 10;
    /**
     * @var int 尝试连接时等待的秒数。使用0表示无限期等待。
     */
    protected int $connectTimeout = 5;
    /**
     * @var bool true表示在输出中包含响应头
     */
    protected bool $enableResponseHeader = false;

    /**
     * 解析Http-Headers
     */
    public function parseHttpHeaders(string $headerString): array
    {
        $headers = [];

        $headerLines = explode("\r\n", rtrim($headerString, "\r\n"));

        array_shift($headerLines);

        foreach ($headerLines as $line)
        {
            [$key, $value] = explode(':', $line, 2);
            $headers[$key] = ltrim($value, "\x20");
        }

        return $headers;
    }

    public function send(): WeChatHttpResponse
    {
        $curl = curl_init();
        // 请求url
        curl_setopt($curl, CURLOPT_URL, $this->url);
        // 设置Method
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $this->method);
        // 设置请求头
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->headers);
        // body参数
        curl_setopt($curl, CURLOPT_POSTFIELDS, $this->body);
        // 设置请求超时时间
        curl_setopt($curl, CURLOPT_TIMEOUT, $this->requestTimeout);
        // 设置连接超时时间
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $this->connectTimeout);
        // 获取响应头信息
        curl_setopt($curl, CURLOPT_HEADER, $this->enableResponseHeader);
        // 响应字符串
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        // 不校验SSL证书
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        // 不校验SSL域名
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);

        $response   = curl_exec($curl);
        $httpCode   = curl_getinfo($curl, CURLINFO_RESPONSE_CODE);
        $headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);

        curl_close($curl);

        if ($response === false)
        {
            throw new WeChatHttpException("微信[HTTP]请求失败，请检查网络");
        }

        if ($this->enableResponseHeader)
        {
            // 请求头
            $headerString = substr($response, 0, $headerSize);
            // 请求体
            $responseBody = substr($response, $headerSize);
            // headers
            $headers = $this->parseHttpHeaders($headerString);

            return new WeChatHttpResponse($responseBody, $headers, $httpCode);
        }

        return new WeChatHttpResponse($response, [], $httpCode);
    }

}
