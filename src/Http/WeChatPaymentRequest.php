<?php

namespace WeChat\Http;

use WeChat\Attribute\WeChatAttribute;
use WeChat\Base\WeChatRequest;
use WeChat\Config\WeChatConfig;
use WeChat\Exception\WeChatParamsException;

class WeChatPaymentRequest extends WeChatHttpRequest
{

    private WeChatConfig $config;
    private WeChatAttribute $attribute;

    /**
     * @param   WeChatConfig   $config   配置信息
     * @param   WeChatRequest  $request  请求信息
     */
    public function __construct(WeChatConfig $config, WeChatRequest $request)
    {
        if (empty($config->mchId))
        {
            throw new WeChatParamsException('商户号不能为空');
        }
        if (empty($config->mchKey))
        {
            throw new WeChatParamsException('商户密钥不能为空');
        }
        if (empty($config->serialNo))
        {
            throw new WeChatParamsException('证书序列号不能为空');
        }
        if (empty($config->privateKey))
        {
            throw new WeChatParamsException('证书私钥不能为空');
        }
        $this->config    = $config;
        $this->attribute = $request->getAttribute($config);
    }

    /**
     * API-V3 Authorization
     * <p>
     * https://pay.weixin.qq.com/wiki/doc/apiv3/wechatpay/wechatpay4_0.shtml
     */
    private function authorization(): string
    {
        $timestamp    = time();
        $nonceStr     = uniqid() . rand();
        $method       = $this->attribute->getMethod();
        $signatureURL = $this->attribute->getSignatureURL();
        $requestBody  = $this->attribute->getRequestBody();
        $message      = "$method\n$signatureURL\n$timestamp\n$nonceStr\n$requestBody\n";

        $privateKey = openssl_pkey_get_private($this->config->privateKey);
        openssl_sign($message, $signature, $privateKey, OPENSSL_ALGO_SHA256);

        $schema    = 'WECHATPAY2-SHA256-RSA2048';
        $mchId     = $this->config->mchId;
        $serialNo  = $this->config->serialNo;
        $signature = base64_encode($signature);

        return "$schema mchid=\"$mchId\",serial_no=\"$serialNo\",nonce_str=\"$nonceStr\",timestamp=\"$timestamp\",signature=\"$signature\"";
    }

    /**
     * API-V3 Headers
     */
    private function getHeaders(): array
    {
        return [
            'Accept: application/json',
            'User-Agent: wechat-sdk-php',
            'Content-Type: application/json',
            'Authorization: ' . $this->authorization(),
        ];
    }

    public function execute(): WeChatHttpResponse
    {
        $this->enableResponseHeader = true;
        $this->headers              = $this->getHeaders();
        $this->method               = $this->attribute->getMethod();
        $this->url                  = $this->attribute->getRequestURL();
        $this->body                 = $this->attribute->getRequestBody();

        return $this->send();
    }

}
