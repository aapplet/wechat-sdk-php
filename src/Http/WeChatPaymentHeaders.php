<?php

namespace WeChat\Http;

/**
 * 微信支付v3响应头验签数据
 */
class WeChatPaymentHeaders
{

    /**
     * 应答请求ID
     */
    protected string $requestID;
    /**
     * 应答随机串
     */
    protected string $nonce;
    /**
     * 应答证书序列号
     */
    protected string $serial;
    /**
     * 应答签名串
     */
    protected string $signature;
    /**
     * 应答时间戳
     */
    protected string $timestamp;
    /**
     * 应答签名类型
     */
    protected string $signatureType;

    /**
     * @return string 应答请求ID
     */
    public function getRequestID(): string
    {
        return $this->requestID;
    }

    /**
     * @return string 应答随机串
     */
    public function getNonce(): string
    {
        return $this->nonce;
    }

    /**
     * @return string 应答证书序列号
     */
    public function getSerial(): string
    {
        return $this->serial;
    }

    /**
     * @return string 应答签名串
     */
    public function getSignature(): string
    {
        return $this->signature;
    }

    /**
     * @return string 应答时间戳
     */
    public function getTimestamp(): string
    {
        return $this->timestamp;
    }

    /**
     * @return string 应答签名类型
     */
    public function getSignatureType(): string
    {
        return $this->signatureType;
    }

    /**
     * @return bool 有没有证书序列号
     */
    public function emptySerialNumber(): bool
    {
        return $this->serial === '';
    }

    /**
     * @param   array  $headers  响应头
     */
    public function __construct(array $headers)
    {
        $this->requestID     = $headers['Request-ID'] ?? '';
        $this->nonce         = $headers['Wechatpay-Nonce'] ?? '';
        $this->serial        = $headers['Wechatpay-Serial'] ?? '';
        $this->signature     = $headers['Wechatpay-Signature'] ?? '';
        $this->timestamp     = $headers['Wechatpay-Timestamp'] ?? '';
        $this->signatureType = $headers['Wechatpay-Signature-Type'] ?? '';
    }

}
