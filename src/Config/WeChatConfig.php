<?php

namespace WeChat\Config;

use Closure;

/**
 * 配置文件
 */
class WeChatConfig
{
    /**
     * 公众平台应用ID
     */
    public string $appId;
    /**
     * 公众平台应用秘钥
     */
    public string $appSecret;
    /**
     * 商户号
     */
    public string $mchId;
    /**
     * 商户秘钥
     */
    public string $mchKey;
    /**
     * 证书序列号
     */
    public string $serialNo;
    /**
     * 证书私钥
     */
    public string $privateKey;
    /**
     * 服务ID
     */
    public string $serviceId;
    /**
     * 支付回调地址
     */
    public string $payNotifyUrl;
    /**
     * 退款回调地址
     */
    public string $refundNotifyUrl;
    /**
     * 支付分回调地址
     */
    public string $payScoreNotifyUrl;

    /**
     * 自定义获取平台证书方法
     *
     * @param   string  $serialNumber  平台证书序列号
     *
     * @return string 平台证书
     */
    public ?Closure $getCertificate = null;

    /**
     * 自定义保存平台证书方法
     *
     * @param   string  $serialNumber  平台证书序列号
     * @param   string  $certificate   平台证书
     * @param   string  $expireTime    过期时间
     *
     * @return void
     */
    public ?Closure $setCertificate = null;

}
