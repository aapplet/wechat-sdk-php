<?php

namespace WeChat\Request;

use WeChat\Attribute\WeChatAttribute;
use WeChat\Attribute\WeChatPaymentAttribute;
use WeChat\Base\WeChatRequest;
use WeChat\Config\WeChatConfig;
use WeChat\Exception\WeChatParamsException;

/**
 * 关闭订单API
 * <p>
 * https://pay.weixin.qq.com/wiki/doc/apiv3/apis/chapter3_5_3.shtml
 */
class WeChatPaymentCloseRequest implements WeChatRequest
{

    /**
     * 直连商户号
     */
    public string $mchid;

    /**
     * 商户订单号
     */
    public ?string $out_trade_no;

    public function __construct(?string $out_trade_no = null)
    {
        $this->out_trade_no = $out_trade_no;
    }

    public function getAttribute(WeChatConfig $config): WeChatAttribute
    {
        if (empty($this->out_trade_no))
        {
            throw new WeChatParamsException("商户订单号不能为空");
        }

        $this->mchid ??= $config->mchId;

        $attribute = new WeChatPaymentAttribute();
        $attribute->setMethod('POST');
        $attribute->setPath('/v3/pay/transactions/out-trade-no/' . $this->out_trade_no . '/close');
        $attribute->setRequestBody(json_encode(array('mchid' => $this->mchid)));

        return $attribute;
    }

}
