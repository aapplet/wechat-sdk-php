<?php

namespace WeChat\Request;

use WeChat\Attribute\WeChatAttribute;
use WeChat\Attribute\WeChatPaymentAttribute;
use WeChat\Base\WeChatRequest;
use WeChat\Config\WeChatConfig;
use WeChat\Exception\WeChatParamsException;

/**
 * 查询订单API
 * <p>
 * https://pay.weixin.qq.com/wiki/doc/apiv3/apis/chapter3_5_2.shtml
 */
class WeChatPaymentQueryRequest implements WeChatRequest
{

    /**
     * 直连商户号
     */
    public string $mchid;
    /**
     * 商户订单号
     */
    public ?string $out_trade_no;
    /**
     * 微信支付订单号
     */
    public ?string $transaction_id;

    public function __construct(?string $out_trade_no = null, ?string $transaction_id = null)
    {
        $this->out_trade_no   = $out_trade_no;
        $this->transaction_id = $transaction_id;
    }

    /**
     * 请求路径
     */
    public function getRequestPath(): string
    {
        // 微信支付订单号查询
        if (!empty($this->transaction_id))
        {
            return "/v3/pay/transactions/id/" . $this->transaction_id;
        }
        // 商户订单号查询
        if (!empty($this->out_trade_no))
        {
            return "/v3/pay/transactions/out-trade-no/" . $this->out_trade_no;
        }
        throw new WeChatParamsException("商户订单号和微信支付订单号都是空的");
    }

    public function getAttribute(WeChatConfig $config): WeChatAttribute
    {
        $this->mchid ??= $config->mchId;

        $attribute = new WeChatPaymentAttribute();
        $attribute->setMethod('GET');
        $attribute->setPath($this->getRequestPath());
        $attribute->setParameters("mchid=" . $this->mchid);

        return $attribute;
    }

}
