<?php

namespace WeChat\Request;

use WeChat\Attribute\WeChatAttribute;
use WeChat\Attribute\WeChatPlatformAttribute;
use WeChat\Base\WeChatRequest;
use WeChat\Config\WeChatConfig;
use WeChat\Exception\WeChatParamsException;

/**
 * 小程序登录
 * <p>
 * https://developers.weixin.qq.com/miniprogram/dev/OpenApiDoc/user-login/code2Session.html
 */
class AppletCode2SessionRequest implements WeChatRequest
{

    /**
     * 小程序 appId
     */
    public string $appid;
    /**
     * 小程序 appSecret
     */
    public string $secret;
    /**
     * 登录时获取的 code，可通过wx.login获取
     */
    public ?string $js_code;
    /**
     * 授权类型，此处只需填写 authorization_code
     */
    public string $grant_type = 'authorization_code';

    public function __construct(?string $js_code = null)
    {
        $this->js_code = $js_code;
    }

    public function getAttribute(WeChatConfig $config): WeChatAttribute
    {
        if (empty($this->js_code))
        {
            throw new WeChatParamsException("code不能为空");
        }

        $this->appid  ??= $config->appId;
        $this->secret ??= $config->appSecret;

        $attribute = new WeChatPlatformAttribute();
        $attribute->setMethod('GET');
        $attribute->setPath('/sns/jscode2session');
        $attribute->setParameters(http_build_query([
            'appid'      => $this->appid,
            'secret'     => $this->secret,
            'js_code'    => $this->js_code,
            'grant_type' => $this->grant_type
        ]));

        return $attribute;
    }

}
